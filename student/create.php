<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// get database connection
include_once '../config/db.php';
 
// instantiate student object
include_once 'student.php';
 
$database = new Database();
$db = $database->getConnection();
 
$student = new Student($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));
 
// make sure data is not empty
if(
    !empty($data->Name) && !empty($data->Regno) && !empty($data->Year) 
  )
{
	$student->Name = $data->Name;
    $student->Regno = $data->Regno;
    $student->Year = $data->Year;

 if($student->create_student())

 {
     	http_response_code(201);
 		echo json_encode(array("message" => "Student was created."));
 }

 else

 {
 
	http_response_code(503);
	echo json_encode(array("message" => "Unable to create Student."));
 }

}
?>
