<?php
class Student{
 
    // database connection and table name
    private $conn;
    private $table_name = "student1";
 
    // object properties
    public $id;
    public $Name;
    public $Regno;
    public $Year;
 

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

function create_student()
	{

		$query = "INSERT INTO " . $this->table_name . " SET id=:id,Name=:Name, Regno=:Regno, Year=:Year";
		$stmt = $this->conn->prepare($query);
    $this->id=htmlspecialchars(strip_tags($this->id));
    $this->Name=htmlspecialchars(strip_tags($this->Name));
    $this->Regno=htmlspecialchars(strip_tags($this->Regno));
    $this->Year=htmlspecialchars(strip_tags($this->Year));
 
    // bind values
    $stmt->bindParam(":id",$this->id);
    $stmt->bindParam(":Name", $this->Name);
    $stmt->bindParam(":Regno", $this->Regno);
    $stmt->bindParam(":Year", $this->Year);
    // execute query
    if($stmt->execute()){
        return true;
    }

    return false;
	}
function read_student(){
		$query="SELECT * FROM " . $this->table_name . "";
		 // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        return $stmt;
	}
function update_student(){
		  $query = "UPDATE  " . $this->table_name . " SET id = :id,Name = :Name,Regno= :Regno,Year = :Year
            WHERE id = :id";
		 $stmt = $this->conn->prepare($query);

    $this->id=htmlspecialchars(strip_tags($this->id));
	$this->Name=htmlspecialchars(strip_tags($this->Name));
    $this->Regno=htmlspecialchars(strip_tags($this->Regno));
    $this->Year=htmlspecialchars(strip_tags($this->Year));
 
    // bind values
    $stmt->bindParam(":id", $this->id);
    $stmt->bindParam(":Name", $this->Name);
    $stmt->bindParam(":Regno", $this->Regno);
    $stmt->bindParam(":Year", $this->Year);
	// execute the query
    if($stmt->execute()){
        return true;
    }
 
    return false;
	}
function delete_student()
{
	// delete query
    $query = "DELETE FROM " . $this->table_name . " WHERE id= ?";
 
    // prepare query
    $stmt = $this->conn->prepare($query);
 
    // sanitize
    $this->id=htmlspecialchars(strip_tags($this->id));
 
    // bind id of record to delete
    $stmt->bindParam(1, $this->id);
 
    // execute query
    if($stmt->execute()){
        return true;
    }
 
    return false;
}
}
?>
`