$(document).ready(function(){
    $(document).on('click', '#create-student-button', function(){
        $.getJSON("http://localhost/student/student/create.php", function(data){
var create_student_html=`
    <div  class='btn btn-primary pull-right m-b-15px' id='read-student-button'>
        <span class='glyphicon glyphicon-list'></span>Read Students
    </div>

    
<form id='create-student-form' action='#' method='post' border='0'>
    <table class='table table-hover table-responsive table-bordered'>
 
        <!-- name field -->
        <tr>
            <td>Name</td>
            <td><input type='text' name='Name' class='form-control' required /></td>
        </tr>
       
        <tr>
            <td>Regno</td>
            <td><input type='varchar' name='Regno' class='form-control' required /></td>
        </tr>
        <tr>
            <td>Year</td>
            <td><input type='number' name='Year' class='form-control' required /></td>
        </tr>
 
        <!-- button to submit form -->
        <tr>
            <td></td>
            <td>
                <button type='submit' class='btn btn-primary'>
                    <span class='glyphicon glyphicon-plus'></span> Create Student
                </button>
            </td>
        </tr>
 
    </table>
</form>`;
// inject html to 'page-content' of our app
$("#page-content").html(create_student_html);
 
// chage page title
changePageTitle("Create Student");
 
});
        $(document).on('submit', '#create-student-form', function(){
                var form_data=JSON.stringify($(this).serializeObject());
    // submit form data to api
$.ajax({
    url: "http://localhost/student/student/create.php",
    type : "POST",
    contentType : 'application/json',
    data : form_data,
    success : function(result) {
        showStudents();
    },
    error: function(xhr, resp, text) {
        // show error to console
        console.log(xhr, resp, text);
    }
});
 
return false;

});
});
   // will run if create student form was submitted
});