$(document).ready(function(){
    $(document).on('click', '#update-student-button', function(){
var id = $(this).attr('data-id');
$.getJSON("http://localhost/student/student/read.php?id=" + id, function(data){
    var Name = data.Name;
    var Regno = data.Regno;
    var Year = data.Year;
$.getJSON("http://localhost/student/student/read.php", function(data){
        var student_options_html=`<select name='Name' class='form-control'>`;
 
        $.each(data.records, function(key, val){
            if(val.id==Name){
         student_options_html+=`<option value='` + val.id + `' selected>` + val.Name + `</option>`; 
                             }
 
            else
                { 
                    student_options_html+=`<option value='` + val.id + `'>` + val.Name + `</option>`; 
                }
        });
        student_options_html+=`</select>`;
var update_student_html=`
    <div id='read-student-button' class='btn btn-primary pull-right m-b-15px'>
        <span class='glyphicon glyphicon-list'></span>Read Students
    </div>
<form id='update-student-form' action='#' method='post' border='0'>
    <table class='table table-hover table-responsive table-bordered'>
 
        <!-- name field -->
        <tr>
            <td>Name</td>
            <td><input value=\"` +Name + `\" type='text' name='Name' class='form-control' required /></td>
        </tr>
 
        <!-- price field -->
        <tr>
            <td>Regno</td>
            <td><input value=\"` + Regno + `\" type='Varchar' name='Regno' class='form-control' required /></td>
        </tr>
        <tr>
            <td>Year</td>
            <td><input value=\"` + Year + `\" type='Year' min='2010' name='Year' class='form-control' required /></td>
        </tr>
        <tr>
            <td><input value=\"` + id + `\" name='id' type='hidden' /></td>
 
            <!-- button to submit form -->
            <td>
                <button type='submit' class='btn btn-info'>
                    <span class='glyphicon glyphicon-edit'></span>Update Student
                </button>
            </td>
 
        </tr>
 
    </table>
</form>`;
// inject to 'page-content' of our app
$("#page-content").html(update_student_html);
 
// chage page title
changePageTitle("Update Student");
     
});
});
    });
     
$(document).on('submit', '#update-student-form', function(){
     
    var form_data=JSON.stringify($(this).serializeObject());
    // submit form data to api
$.ajax({
    url: "http://localhost/student/student/update.php",
    type : "POST",
    contentType : 'application/json',
    data : form_data,
    success : function(result) {
              showStudents();
    },
    error: function(xhr, resp, text) {
        // show error to console
        console.log(xhr, resp, text);
    }
});
     
    return false;
});
});