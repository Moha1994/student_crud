$(document).ready(function(){
    showStudents();
$(document).on('click', '#read-student-button', function(){
    showStudents();
});
});
function showStudents(){
$.getJSON("http://localhost/student/student/read.php", function(data){
var read_html=`
    <div id='create-student-button' class='btn btn-primary pull-right m-b-15px'>
        <span class='glyphicon glyphicon-plus'></span> Create Student
    </div>
    <!-- start table -->
<table class='table table-bordered table-hover'>
 
    <!-- creating our table heading -->
    <tr>
        <th class='w-15-pct'>Name</th>
        <th class='w-15-pct'>Regno</th>
        <th class='w-15-pct'>Year</th>
        <th class='w-25-pct text-align-center'>Action</th>
    </tr>`;
    // loop through returned list of data
$.each(data.records,function(key, val) {
    // creating new table row per record
    console.log(val.Name);
    read_html+=`
        <tr>
            <td>` + val.Name + `</td>
            <td>$` + val.Regno + `</td>
            <td>` + val.Year + `</td>
 
            <!-- 'action' buttons -->
            <td>
                <button class='btn btn-primary m-r-10px'id='read-student-button' data-id='` + val.id + `'>
                    <span class='glyphicon glyphicon-eye-open'></span> Read
                </button>
 
                <!-- edit button -->
                <button class='btn btn-info m-r-10px'id='update-student-button'  data-id='` + val.id + `'>
                    <span class='glyphicon glyphicon-edit'></span> Edit
                </button>
 
                <!-- delete button -->
                <button class='btn btn-danger' id='delete-student-button' data-id='` + val.id + `'>
                    <span class='glyphicon glyphicon-remove'></span> Delete
                </button>
            </td>
 
        </tr>`;
});
 
  read_html+=`</table>`;
// inject to 'page-content' of our app
$("#page-content").html(read_html);
// chage page title
changePageTitle("Read Students");
});
}